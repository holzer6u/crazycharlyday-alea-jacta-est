<?php
namespace projet\models;
use Illuminate\Database\Eloquent\Model;


class appartient extends Model
{
    protected $table = 'appartient';
    protected $primaryKey = "idGroupe,idUser";
    public $timestamps = false;
}