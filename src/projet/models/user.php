<?php
namespace projet\models;
use Illuminate\Database\Eloquent\Model;

class user extends Model
{
    protected $table = 'user';
    protected $primaryKey = "id";
    public $timestamps = false;

}