<?php

namespace projet\models;
use Illuminate\Database\Eloquent\Model;

class groupe extends Model
{
    protected $table = 'groupe';
    protected $primaryKey = "id";
    public $timestamps = false;
}