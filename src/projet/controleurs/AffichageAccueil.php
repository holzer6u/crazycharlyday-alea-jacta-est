<?php

namespace projet\controleurs;
use projet\models\user;
use projet\vues\VueAccueil;

class AffichageAccueil{

    public static function afficherAccueil(){
        $listg = user::get();
        $vue = new VueAccueil($listg);
        echo $vue->render(1);
    }

}