<?php

namespace projet\controleurs;
use projet\vues\VueGroupe;
use projet\models\groupe;
use projet\models\appartient;
use projet\models\logement;


class AffichageGroupe {

    public static function creationGroupe(){
        if (isset($_SESSION['idUser'])) {
        	//Identifié 
        	$grp = groupe::where('idProprietaire','=',$_SESSION['idUser'])->first();
        	if ($grp != null && $grp->valide!=2) {
        		$vg = new VueGroupe($_SESSION['idUser']);
        		echo $vg->render(3);
        	}else{
        		$vg = new VueGroupe($_SESSION['idUser']);
        		echo $vg->render(2);
        	}
        }else{
        	//Pas identifié
        	$vg = new VueGroupe(1);
        	echo $vg->render(1);
        }
    }

    public function creerGroupe($post){
    	//SELECT * FROM `your_table` order by id desc limit 1
    	$idMax = groupe::orderby('id','desc')->first()->id;
    	if ($idMax == null) {
    		$idMax=1;
    	}
    	$idMax+=1;

    	$groupe = new groupe();
    	$groupe->id = $idMax;
    	$groupe->description = $post['desc'];
    	$groupe->nbColocataire = 1;
    	$groupe->idProprietaire = $_SESSION['idUser'];
    	$groupe->idLogement = $post['idLogement'];
    	
    	$groupe->save();

    	$vg = new VueGroupe($_SESSION['idUser']);
        echo $vg->render(6);
    }

    public function afficherGroupe() {
        if (isset($_SESSION['idUser'])) {
        	//Identifié 
        	$grp = groupe::where('idProprietaire','=',$_SESSION['idUser'])->first();
        	if ($grp != null) {
        		$id=$grp->id;
        		$vg = new VueGroupe(groupe::where("id","=",$id)->first());
        		echo $vg->render(4);
        	}else{
        		$vg = new VueGroupe($_SESSION['idUser']);
        		echo $vg->render(5);
        	}
        }else{
        	//Pas identifié
        	$vg = new VueGroupe(1);
        	echo $vg->render(1);
        }
    }

    public function adjColoc($post) {
    	$grp = groupe::where('id','=',$post['idGrp'])->first();
    	$appart = new appartient();
    	$idlog=$grp->idLogement;
    	$log=logement::where('id','=',$idlog)->first();
    	if ($idlog != null) {
    		$s=$log->places;
    		if ($grp->nbColocataire < $log->places) {
    			$appart->idGroupe=$post['idGrp'];
    			$appart->idUser=$post['idColoc'];
    			$appart->save();
    			
    			$grp->nbColocataire+=1;
    			$grp->save();
    		}
    	}
    	
    	
    }
}