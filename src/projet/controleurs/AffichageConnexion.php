<?php
namespace projet\controleurs;
use projet\vues\VueConnexion;

class AffichageConnexion{

    public static function afficherConnexion($id=null){
        $vue = new VueConnexion();
        echo $vue->render();
    }

    public static function verifierConnexion(){
        $res = false;
        if($_POST["pseudo"] == "admin"){
            if($_POST["mdp"] == "admin"){
                $res = true;
                $_SESSION["admin"]=true;
                unset($_SESSION['idUser']);
            }
        }
        return $res;
    }

    public static function deconnexion(){
        unset($_SESSION["admin"]);
        unset($_SESSION['idUser']);
    }
}