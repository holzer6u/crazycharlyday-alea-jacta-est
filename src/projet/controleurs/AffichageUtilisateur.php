<?php
/**
 * Created by PhpStorm.
 * User: tibo
 * Date: 09/02/2017
 * Time: 10:52
 */

namespace projet\controleurs;
use projet\models\user;
use projet\vues\VueUser;
use Slim\Slim;

class AffichageUtilisateur{

    public static function afficherUtilisateurs(){
        $listg=  user::get();
        $vue = new VueUser($listg);
        echo $vue->render(1);
    }

    public static function afficherUnUtilisateur($id){
        $user = user::where('id','=',$id)->first();
        $vue = new VueUser($user) ;
        echo $vue->render(2);
    }

    public static function noterUser($id,$note){
        if (isset($_SESSION['idUser'])) {
            //Identifié
            $us = User::where('id','=',$id)->first();
            if($note>=1 && $note<=5){
                $use= User::where('id','=',$id)->update(array('note'=>$us->note+ $note,'nbNote'=>$us->nbNote+1));
            }
            $app=\Slim\Slim::getInstance();
            $app->response->redirect($app->urlFor('utilisateur',["id"=>$us->id]));
        }else{
            //Pas identifié
            $vg = new VueUser(1);
            echo $vg->render(4);
        }
    }

}