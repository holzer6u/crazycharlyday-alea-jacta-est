<?php 

namespace projet\controleurs;
use projet\models\logement;
use projet\vues\VueLogement;
class AffichageLogement{
	
	public static function afficherLogement($id=null){
	    if (isset($id)){
            $log = logement::where('id','=',$id)->first();
            $vue = new VueLogement($log);
            echo $vue->render(3);
        }else{
            $log = logement::get();
            $vue = new VueLogement($log);
            echo $vue->render(1);
        }
	}
	
	public static function afficherLogementLibre(){
		$log = logement::where("places",">",0)->get();
		$vue = new VueLogement($log);
		echo $vue->render(2);
	}

    public static function noterLogement($id,$note){
        if (isset($_SESSION['idUser'])) {
            //Identifié
            $us = Logement::where('id','=',$id)->first();
            if($note>=1 && $note<=5){
                $use = Logement::where('id','=',$id)->update(array('note'=>$us->note+ $note,'nbNote'=>$us->nbNote+1));
            }
            $app=\Slim\Slim::getInstance();
            $app->response->redirect($app->urlFor('logement',["id"=>$us->id]));
        }else{
            //Pas identifié
            $vg = new VueUser(1);
            echo $vg->render(4);
        }
    }
}