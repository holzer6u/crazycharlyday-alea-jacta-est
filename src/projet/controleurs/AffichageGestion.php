<?php
namespace projet\controleurs;
use Illuminate\Support\Facades\DB;
use projet\models\appartient;
use projet\models\groupe;
use projet\models\logement;
use projet\vues\VueGestion;
class AffichageGestion{

    public static function afficherGestion(){
        $vue = new VueGestion();
        echo $vue->render();
    }

    public static function refuserGroupe($id){
        groupe::where("id","=",$id)->update(array("valide"=>2));
        appartient::where("idGroupe","=",$id)->delete();
    }

    public static function validerGroupe($id){
        groupe::where("id","=",$id)->update(array("valide"=>1));
        $idLogement = groupe::where("id","=",$id)->first()->idLogement;
        logement::where("id","=",$idLogement)->update(array("places"=>0));
    }
}