<?php
/**
 * Created by PhpStorm.
 * User: Romain KRAFT
 * Date: 19/12/2016
 * Time: 10:28
 */

namespace projet\vues;
use projet\models\groupe;


class Vue
{

    static function render($content)
    {
        $app = \Slim\Slim::getInstance();
        $accueil = $app->urlFor('root');
        $user = $app->urlFor('utilisateurs');
        $creeGroupe = $app->urlFor('creationGroupe');
		$logement = $app->urlFor('logementLibre');
        $kon = $app->urlFor('root');
        $afficherGroupe = $app->urlFor('groupe');
        $gestion = $app->urlFor('gestion');
        $deconnexion = $app->urlFor('deconnexion');
        $connexion = $app->urlFor('connexion');


        $html = <<<END
        <!DOCTYPE html>
        <html>
        <head>
            <meta charset='utf-8'>
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
            <link rel="stylesheet" href="${accueil}include/css/stylesheet.css" />
            <title>Crazy Charly Day</title>
            <link rel="shortcut icon" href="${accueil}img/icon/icon.png" />
            <script charset="utf-8" src="http://code.jquery.com/jquery-1.6.1.min.js"></script>
            <script src="${accueil}/web/js/konamiCode.js"></script>
        </head>
        <body>
        <div id="navbar" class="navbar navbar-static-top fixed-nav-bar">
          <ul class="nav navbar-nav">
            <li class="active"><a href="${accueil}">Accueil</a></li>
            <li><a href="${user}">Utilisateurs</a></li>
			<li><a href="${logement}">Logements libres</a></li>
            <li><a href="${creeGroupe}">Creer groupe</a></li>
            <li><a href="${afficherGroupe}">Mon groupe</a></li>
          </ul>
          <ul class="nav navbar-nav navbar-right">
END;
        if(array_key_exists('admin', $_SESSION)){
            $html .= "<li><a href=\"${gestion}\">Gestion groupe</a></li>";
            $html .= "<li><a href=\"${deconnexion}\">Déconnexion</a></li>";
        }else {
            $html .= "<li><a href=\"${connexion}\">Connexion</a></li>";
        }

        $html .= <<<END
        </ul>
        </div>
        <div class="container">
            $content
        </div>
        <div class="footer">
            <p> ALEA JACTA EST - BAILLY / BARTHELEMY / BENIER / HOLZER / MARBACH </p>
        </div>
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <!-- Latest compiled and minified JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
        </body>
        
        </html>
END;
        return $html;
    }

}