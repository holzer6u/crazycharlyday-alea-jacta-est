<?php

namespace projet\vues;



use projet\models\user;

class VueAccueil
{
	private $content;
    private $listeObjet;

    public function __construct($content){
        $this->listeObjet = $content;
    }

    public function render($type)
    {
        switch ($type) {
            case 1:
                $this->content = $this->afficherAccueil();
                break;
            default:
                # code...
                break;
        }
        

        $html = Vue::render($this->content);
        echo $html;
    }

    private function afficherAccueil()
    {
        if(isset($_SESSION['idUser'])) {
            $nom = user::select("nom")->where('id',"=",$_SESSION['idUser'])->first();
            $this->content = <<<END
<p> Vous êtes connecté en tant que $nom->nom </p>
END;
            $this->content .= "<h1>Identification :</h1>";
        }else{
            $this->content = "<h1>Identification :</h1>";
        }
        $app = \Slim\Slim::getInstance();

        foreach ($this->listeObjet as $tmp){
            $conec = $app->urlFor('connection',['id'=>$tmp->id]);
            $this->content.="<a href=$conec>";
            $img = $app->urlFor('root');
            $this->content .= <<<END
            <div class="cadreUser">
                <div class="photo">
                    <img src='${img}image/user/$tmp->id.jpg' alt="">
                </div> 
                
                <div class="descr">
                   <div class="nom"> <p> $tmp->nom </p> </div>
                </div>
            </div>
            <br><br></a>
END;
        }
    	return $this->content;
    }
}