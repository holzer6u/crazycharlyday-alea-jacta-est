<?php

namespace projet\vues;

class VueConnexion
{
    private $content;



    public function _construct()
    {

    }

    public function render()
    {
        $app = \Slim\Slim::getInstance();
        $validerConnexion = $app->urlFor('validerConnexion');
        $this->content .= <<<END
        <div class="cadreCon">
            <form action="${validerConnexion}" method="post"> 
            <label for='pseudo'>Pseudo : </label>
            <input type="text" name="pseudo" id="pseudo" required/> <br> <br>
            <label for="mdp">Mot de passe : </label>
            <input type="password" name="mdp" id="mdp" required/> <br> <br>
     
            <input type="submit" name="connexion" value="Connexion" />
            </form>    
        </div>
END;
        $html = Vue::render($this->content);
        return $html;
    }
}