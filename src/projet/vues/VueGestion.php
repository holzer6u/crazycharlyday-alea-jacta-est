<?php

namespace projet\vues;

use projet\models\appartient;
use projet\models\groupe;
use projet\models\user;

class VueGestion
{

    private $content;
    private $listeGroupe;


    public function render()
    {
        $listeGroupe = groupe::where("valide","=",0)->get();
        $app = \Slim\Slim::getInstance();
        $img = $app->urlFor('root');

        foreach ($listeGroupe as $g) {
            $validerGroupe = $app->urlFor('validerGroupe',["id"=>$g->id]);
            $refuserGroupe = $app->urlFor('refuserGroupe',["id"=>$g->id]);
            $idMaison = groupe::where("id", "=", $g->id)->first()->idLogement;
            $descr = groupe::where("id", "=", $g->id)->first()->description;
            $idGroupe = $g->id;
            $this->content .= <<<END
            <div class="cadreGroupe">
                     <div class="Maison">
                        <img src=${img}image/appart/$idMaison.jpg alt="">
                     </div>
                     <div class="Detail">
                        $descr
                     </div>
END;
            $personnes = appartient::where("idgroupe", "=", $idGroupe)->get();
            foreach ($personnes as $p) {
                $idpers = $p->idUser;
                $this->content .= <<<END
                
                <div class="Personne">
                    <img src=${img}/image/user/$idpers.jpg alt="">
                </div>
            
END;
            }
            $this->content .= <<<END
                <form action="${validerGroupe}" method="post">
                    <input type="submit" name="Valider" value="Valider" />
                </form>
                <form action="${refuserGroupe}" method="post">
                    <input type="submit" name="Refuser" value="Refuser" />
                </form>
            </div>
            <br> <br>
END;
        }

        $html = Vue::render($this->content);
        return $html;
    }

}