<?php


namespace projet\vues;

class VueLogement{

    private $content;
    private $listeObjet;

    public function __construct($arrayPresta){
        $this->listeObjet = $arrayPresta;
    }
	public function afficherLogement(){
		$app = \Slim\Slim::getInstance();
        foreach ($this->listeObjet as $tmp){
            $img = $app->urlFor('root');
            $logement = $app->urlFor('logement',['id'=>$tmp->id]);
            $this->content .="<a href=${logement}>";
			$this->content.= <<<END
			<div class="cadreLogement">
				<div class="photo">
					<img src='${img}/image/appart/$tmp->id.jpg' alt="">
				</div>
				<div class= "nbPlace">
					<p>Nombre de places : $tmp->places </p>
				</div>
			</div></a>
			<br> <br>
END;
        }
	}

	public function afficherLogementLibre(){
        $app = \Slim\Slim::getInstance();
        foreach ($this->listeObjet as $tmp){
            $img = $app->urlFor('root');
            $logement = $app->urlFor('logement',['id'=>$tmp->id]);
            $this->content .="<a href=${logement}>";
            $this->content.= <<<END
			<div class="cadreLogement">
				<div class="photo">
					<img src='${img}/image/appart/$tmp->id.jpg' alt="">
				</div>
				<div class= "nbPlace">
					<p>Nombre de places : $tmp->places </p>
				</div>
			</div></a>
			<br> <br>
END;
        }
	}

	public function afficherUnLogement(){
        $app = \Slim\Slim::getInstance();
        $tmp=$this->listeObjet;
        $img = $app->urlFor('root');
        $no1 = $app->urlFor('noterLogement',['id'=>$tmp->id,'note'=>1]);
        $no2 = $app->urlFor('noterLogement',['id'=>$tmp->id,'note'=>2]);
        $no3 = $app->urlFor('noterLogement',['id'=>$tmp->id,'note'=>3]);
        $no4 = $app->urlFor('noterLogement',['id'=>$tmp->id,'note'=>4]);
        $no5 = $app->urlFor('noterLogement',['id'=>$tmp->id,'note'=>5]);
        if($tmp->nbNote==0){
            $lanote = 'Pas de note pour l\'instant';
        }else{
            $lanote = 'Note: '. round($tmp->note/$tmp->nbNote,2);
        }
		$this->content.= <<<END
			<div class="cadreLogement">
				<div class="photo">
					<img src='${img}/image/appart/$tmp->id.jpg' alt="" width=200% height=200%>
				</div>
				<div class= "nbPlace">
					<p>Nombre de places : $tmp->places </p> <p>$lanote</p>
				</div>
				<div class="rating rating2"><!--
                --><a href='${no5}' title="Give 5 stars">★</a><!--
                --><a href='${no4}' title="Give 4 stars">★</a><!--
                --><a href='${no3}' title="Give 3 stars">★</a><!--
                --><a href='${no2}' title="Give 2 stars">★</a><!--
                --><a href='${no1}' title="Give 1 star">★</a>
                </div>
			</div>
END;
    }

	public function render($methode)
    {

        switch ($methode) {
            case 1:
                $this->afficherLogement();
                break;
            case 2:
                $this->afficherLogementLibre();
                break;
            case 3:
                $this->afficherUnLogement();
                break;
            case 4:
                $this->content="<h1>Vous n\'êtes pas connecté</h1>";
                break;
        }
        $html = Vue::render($this->content);
        return $html;
    }
}