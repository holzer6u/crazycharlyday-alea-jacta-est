<?php
namespace projet\vues;

use projet\models\logement;
use projet\models\user;

class VueGroupe
{

    private $content;
    private $listeObjet;

    public function __construct($content){
        $this->listeObjet = $content;
    }
    
    public function render($methode) {
        switch ($methode) {
            case 1:
                $this->content="<h1>Vous n'etes pas connecté</h1>";
                break;
            case 2:
                $this->content= $this->creationGroupe();
                break;
            case 3:
                $this->content="<h1>Vous avez déjà un groupe</h1>";
                break;
            case 4:
                $this->content= $this->afficherGroupe();
                break;
            case 5:
                $this->content="<h1>Vous n'avez pas de groupe</h1>";
                break;
            case 6:
                $this->content="<h1>Vous venez de creer un groupe avec succes</h1>";
                break;
            default:
                break;
        }

        $html = Vue::render($this->content);
        return $html;
    }

    private function creationGroupe(){
        $this->content = "";
        $app = \Slim\Slim::getInstance();
        $creerGroupe = $app->urlFor('creerGroupe');
        $this->content .= <<<END
                <div>
                    <form action='$creerGroupe' method='POST'>
                    <p>Description du groupe de colocation :</p><br>
                    <input type='text' name='desc' id='desc' value='' required><br><br>
END;
        $logements = logement::get();
        foreach ($logements as $tmp){
                    $img = $app->urlFor('root');
                    $this->content .="<button name=\"log\" id=\"log\" type=\"submit\" class=\"customButton\">";
                    $this->content.= <<<END
                    <input type='hidden' name='idLogement' value='$tmp->id'>
                    <div class="cadreLogement">
                        <div class="photo">
                            <img src='${img}/image/appart/$tmp->id.jpg' alt="">
                        </div>
                        <div class= "nbPlace">
                            <p>Nombre de places : $tmp->places </p>
                        </div>
                    </div></button> 
                    <br> <br>
END;
        }
        $this->content .= "</div>";
        return $this->content;
    }

    private function afficherGroupe() {
        $app = \Slim\Slim::getInstance();
        $this->content = "";
        $lo=$this->listeObjet;
        $adjColoc = $app->urlFor('adjColoc');
        $this->content .= <<<END
                <div>
                    <form action='$adjColoc' method='POST'>
                    <p>Votre groupe</p><br>
                    <p>Description : $lo->description</p><br>
                    <p>Nombre de colocataire : $lo->nbColocataire</p><br>
                
END;
        foreach (user::get() as $tmp){
            $img = $app->urlFor('root');
            $this->content .="<button name=\"adjColoc\" id=\"adjColoc\" type=\"submit\" class=\"customButton\">";
            $this->content .= <<<END
            <input type='hidden' name='idColoc' value='$tmp->id'>
            <input type='hidden' name='idGrp' value='$lo->id'>
            <div class="cadreUser">
                <div class="photo">
                    <img src='${img}image/user/$tmp->id.jpg' alt="">
                </div> 
                <div class="descr">
                   <div class="nom"> <p> $tmp->nom </p> </div> <div class="msg"> <p> $tmp->message </p>  </div>
                </div>
            </div></button>
            <br> <br>
END;
        }
        $this->content.="</div>";
        return $this->content;
    }

}