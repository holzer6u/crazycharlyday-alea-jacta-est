<?php
/**
 * Created by PhpStorm.
 * User: tibo
 * Date: 09/02/2017
 * Time: 10:55
 */

namespace projet\vues;


class VueUser
{

    private $content;
    private $listeObjet;

    public function __construct($content){
        $this->listeObjet = $content;
    }

    private function afficherUsers(){
        $app = \Slim\Slim::getInstance();
        foreach ($this->listeObjet as $tmp){
            $img = $app->urlFor('root');
            $utiisateur = $app->urlFor('utilisateur',['id'=>$tmp->id]);
            $this->content .="<a href=${utiisateur}>";
            $this->content .= <<<END
            <div class="cadreUser">
                <div class="photo">
                    <img src='${img}image/user/$tmp->id.jpg' alt="">
                </div> 
                <div class="descr">
                   <div class="nom"> <p> $tmp->nom </p> </div> <div class="msg"> <p> $tmp->message </p>  </div>
                </div>
            </div></a>
            <br> <br>
END;
            /*
            $this->content.=$tmp->id."  ";
            $this->content.=$tmp->nom."  ";
            $this->content.=$tmp->message."<br>";
            */
        }
    }

    private function afficherUnUser(){
        $app = \Slim\Slim::getInstance();
        $tmp = $this->listeObjet;
        $img = $app->urlFor('root');
        $no1 = $app->urlFor('noterUser',['id'=>$tmp->id,'note'=>1]);
        $no2 = $app->urlFor('noterUser',['id'=>$tmp->id,'note'=>2]);
        $no3 = $app->urlFor('noterUser',['id'=>$tmp->id,'note'=>3]);
        $no4 = $app->urlFor('noterUser',['id'=>$tmp->id,'note'=>4]);
        $no5 = $app->urlFor('noterUser',['id'=>$tmp->id,'note'=>5]);
        if($tmp->nbNote==0){
            $lanote = 'Pas de note pour l\'instant';
        }else{
            $lanote = 'Note: '. round($tmp->note/$tmp->nbNote,2);
        }
        $this->content .= <<<END
            <div class="cadreUser">
                <div class="photo">
                    <img src='${img}image/user/$tmp->id.jpg' alt="">
                </div> 
                <div class="descr">
                   <div class="nom"> <p> $tmp->nom </p> </div> <div class="msg"> <p> $tmp->message </p> <p>$lanote</p></div>
                </div>
                <div class="rating rating2"><!--
                --><a href='${no5}' title="Give 5 stars">★</a><!--
                --><a href='${no4}' title="Give 4 stars">★</a><!--
                --><a href='${no3}' title="Give 3 stars">★</a><!--
                --><a href='${no2}' title="Give 2 stars">★</a><!--
                --><a href='${no1}' title="Give 1 star">★</a>
                </div>
            </div>
            <br> <br>
END;
    }
    
    public function render($methode) {
        switch ($methode) {
            case 1:
                $this->afficherUsers();
                break;
            case 2:
                $this->afficherUnUser();
                break;
            case 3:
                $this->afficherUnUser();
                break;
            case 4:
                $this->content="<h1>Vous n\'êtes pas connecté</h1>";
                break;
        }
        $html = Vue::render($this->content);
        return $html;
    }

}