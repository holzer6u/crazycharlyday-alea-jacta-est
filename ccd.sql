-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Jeu 09 Février 2017 à 22:01
-- Version du serveur :  10.1.16-MariaDB
-- Version de PHP :  5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `ccd`
--

-- --------------------------------------------------------

--
-- Structure de la table `appartient`
--

CREATE TABLE `appartient` (
  `idGroupe` int(11) NOT NULL,
  `idUser` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `appartient`
--

INSERT INTO `appartient` (`idGroupe`, `idUser`) VALUES
(0, 2),
(0, 8),
(0, 16);

-- --------------------------------------------------------

--
-- Structure de la table `groupe`
--

CREATE TABLE `groupe` (
  `id` int(11) NOT NULL,
  `description` varchar(255) NOT NULL,
  `nbColocataire` int(11) NOT NULL,
  `idProprietaire` int(11) NOT NULL,
  `idLogement` int(11) DEFAULT NULL,
  `valide` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `groupe`
--

INSERT INTO `groupe` (`id`, `description`, `nbColocataire`, `idProprietaire`, `idLogement`, `valide`) VALUES
(0, 'MDR', 1, 1, 4, 1),
(1, 'Je suis Paul j''aime faire caca chez moi ', 1, 2, 19, 0),
(2, 'BAIIIIIISEEEER !!!!!!', 1, 8, 2, 0);

-- --------------------------------------------------------

--
-- Structure de la table `logement`
--

CREATE TABLE `logement` (
  `id` int(11) NOT NULL,
  `places` int(11) NOT NULL,
  `note` int(11) NOT NULL,
  `nbNote` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `logement`
--

INSERT INTO `logement` (`id`, `places`, `note`, `nbNote`) VALUES
(1, 3, 0, 0),
(2, 3, 0, 0),
(3, 4, 0, 0),
(4, 4, 0, 0),
(5, 4, 0, 0),
(6, 4, 0, 0),
(7, 4, 0, 0),
(8, 5, 0, 0),
(9, 5, 0, 0),
(10, 6, 0, 0),
(11, 6, 0, 0),
(12, 6, 0, 0),
(13, 7, 0, 0),
(14, 7, 0, 0),
(15, 8, 0, 0),
(16, 2, 0, 0),
(17, 2, 0, 0),
(18, 2, 0, 0),
(19, 2, 0, 0),
(20, 2, 0, 0),
(21, 3, 0, 0),
(22, 3, 0, 0),
(23, 3, 0, 0),
(24, 3, 0, 0),
(25, 3, 0, 0);

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `nom` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `message` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `note` int(11) NOT NULL,
  `nbNote` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `user`
--

INSERT INTO `user` (`id`, `nom`, `message`, `note`, `nbNote`) VALUES
(1, 'Jeanne', 'aime la musique ♫', 0, 0),
(2, 'Paul', 'aime cuisiner ♨ ♪', 7, 2),
(3, 'Myriam', 'mange Halal ☪', 0, 0),
(4, 'Nicolas', 'ouvert à tous ⛄', 3, 1),
(5, 'Sophie', 'aime sortir ♛', 0, 0),
(6, 'Karim', 'aime le soleil ☀', 0, 0),
(7, 'Julie', 'apprécie le calme ☕', 0, 0),
(8, 'Etienne', 'accepte jeunes et vieux ☯', 0, 0),
(9, 'Max', 'féru de musique moderne ☮', 0, 0),
(10, 'Sabrina', 'aime les repas en commun ⛵☻', 0, 0),
(11, 'Nathalie', 'bricoleuse ⛽', 0, 0),
(12, 'Martin', 'sportif ☘ ⚽ ⚾ ⛳', 0, 0),
(13, 'Manon', '', 0, 0),
(14, 'Thomas', '', 0, 0),
(15, 'Léa', '', 0, 0),
(16, 'Alexandre', '', 0, 0),
(17, 'Camille', '', 0, 0),
(18, 'Quentin', '', 0, 0),
(19, 'Marie', '', 0, 0),
(20, 'Antoine', '', 0, 0),
(21, 'Laura', '', 0, 0),
(22, 'Julien', '', 0, 0),
(23, 'Pauline', '', 0, 0),
(24, 'Lucas', '', 0, 0),
(25, 'Sarah', '', 0, 0),
(26, 'Romain', '', 0, 0),
(27, 'Mathilde', '', 0, 0),
(28, 'Florian', '', 0, 0);

--
-- Index pour les tables exportées
--

--
-- Index pour la table `appartient`
--
ALTER TABLE `appartient`
  ADD PRIMARY KEY (`idGroupe`,`idUser`),
  ADD KEY `idUser` (`idUser`);

--
-- Index pour la table `groupe`
--
ALTER TABLE `groupe`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idProprietaire` (`idProprietaire`),
  ADD KEY `idLogement` (`idLogement`);

--
-- Index pour la table `logement`
--
ALTER TABLE `logement`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `logement`
--
ALTER TABLE `logement`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT pour la table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `appartient`
--
ALTER TABLE `appartient`
  ADD CONSTRAINT `appartient_ibfk_1` FOREIGN KEY (`idGroupe`) REFERENCES `groupe` (`id`),
  ADD CONSTRAINT `appartient_ibfk_2` FOREIGN KEY (`idUser`) REFERENCES `user` (`id`);

--
-- Contraintes pour la table `groupe`
--
ALTER TABLE `groupe`
  ADD CONSTRAINT `groupe_ibfk_1` FOREIGN KEY (`idProprietaire`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `groupe_ibfk_2` FOREIGN KEY (`idLogement`) REFERENCES `logement` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
