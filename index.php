<?php
require_once 'vendor\autoload.php';
use Illuminate\Database\Capsule\Manager as DB;
use projet\controleurs\AffichageUtilisateur;
use projet\controleurs\AffichageAccueil;
use projet\controleurs\AffichageGroupe;
use projet\controleurs\AffichageConnexion;
use projet\controleurs\AffichageGestion;
use projet\vues\VueAccueil;

use projet\controleurs\AffichageLogement;
use projet\vues\VueUser;
use projet\models\user;


session_start();

$db = new DB();
$db->addConnection(parse_ini_file('src/conf/conf.ini'));
$db->setAsGlobal();
$db->bootEloquent();

$app = new \Slim\Slim();


/* AFFICHER LISTE UTILISATEUR */
$app->get('/user', function(){
    $au = new AffichageUtilisateur();
    $au -> afficherUtilisateurs();
})->name("utilisateurs");

/* AFFICHER UN UTILISATEUR */
$app->get('/user/:id', function($id){
    $au = new AffichageUtilisateur();
    $au -> afficherUnUtilisateur($id);
})->name("utilisateur");

$app->get('/', function(){
    $accueil = new AffichageAccueil();
    $accueil->afficherAccueil();
})->name("root");

/* AFFICHER LISTE LOGEMENT */
$app->get('/logement',function(){
	AffichageLogement::afficherLogement();
})->name("logements");

/* AFFICHER UN LOGEMENT */
$app->get('/logement/:id',function($id){
    AffichageLogement::afficherLogement($id);
})->name("logement");

// ----- Gestion des groupes ----- //
$app->get('/creationGroupe',function (){
	$groupe = new AffichageGroupe();
	$groupe->creationGroupe();
})->name("creationGroupe");

$app->post('/creerGroupe',function (){
	$groupe = new AffichageGroupe();
	$groupe->creerGroupe($_POST);
})->name("creerGroupe");

$app->get('/groupe',function (){
	$groupe = new AffichageGroupe();
	$groupe->afficherGroupe();
})->name("groupe");

$app->post('/adjColoc',function (){
	$groupe = new AffichageGroupe();
	$groupe->adjColoc($_POST);
	
	$app = \Slim\Slim::getInstance();
    $app->response->redirect($app->urlFor('groupe'));
})->name("adjColoc");
// ------------------------------- //

// ----- Connection ----- //
$app->get('/connection/:id',function ($id){
	$_SESSION['idUser']=$id;
	$app = \Slim\Slim::getInstance();
    $app->response->redirect($app->urlFor('root'));
})->name("connection");
// ---------------------- //

/* AFFICHER LISTE LOGEMENT LIBRE */
$app->get('/logementLibre',function(){
    AffichageLogement::afficherLogementLibre();
})->name("logementLibre");


$app->get('/gestion',function(){
    AffichageGestion::afficherGestion();
})->name("gestion");

$app->get('/deconnexion',function(){
    AffichageConnexion::deconnexion();
    $app = \Slim\Slim::getInstance();
    $app->response->redirect($app->urlFor('root'));
})->name("deconnexion");

$app->get('/connexion',function(){
    AffichageConnexion::afficherConnexion();
})->name("connexion");

$app->post('/connexion/valider',function(){
    $res = AffichageConnexion::verifierConnexion();
    $app = \Slim\Slim::getInstance();
    if($res == true) {
        $app->response->redirect($app->urlFor('root'));
    }else{
        $app->response->redirect($app->urlFor('connexion'));
    }
})->name("validerConnexion");

/* NOTER USER */
$app->get('/noter/user/:id/:note',function($id,$note){
    AffichageUtilisateur::noterUser($id,$note);
})->name("noterUser");


$app->post('/groupe/valider/:id', function($id){
    AffichageGestion::validerGroupe($id);
    $app = \Slim\Slim::getInstance();
    $app->response->redirect($app->urlFor('gestion'));
})->name("validerGroupe");

$app->post('/groupe/refuser/:id', function($id){
    AffichageGestion::refuserGroupe($id);
    $app = \Slim\Slim::getInstance();
    $app->response->redirect($app->urlFor('gestion'));
})->name("refuserGroupe");

/* NOTER LOGEMENT */
$app->get('/noter/logement/:id/:note',function($id,$note){
    AffichageLogement::noterLogement($id,$note);
})->name("noterLogement");


$app->run();

